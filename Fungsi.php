<?php
/**
 * Created by PhpStorm.
 * User: vedoalfarizi
 * Date: 19/08/2017
 * Time: 12:27
 */
include 'Koneksi.php';

class Fungsi
{
    protected $db;

    function __construct(){
        $this->db = new Koneksi();
    }

//Kota
    function showAllKota()
    {
        $query = $this->db->kon->query('SELECT * FROM kota');

        return $query;
    }

    function getNamaKota($id)
    {
        $query = $this->db->kon->query('SELECT nama_kota FROM kota WHERE id_kota=\''.$id.'\'');
        $hasil = $query->fetch_assoc();

        return $hasil['nama_kota'];
    }

//Perjalanan
    function showPerjalanan($asal, $tujuan, $tgl)
    {
        $query = $this->db->kon->query('SELECT perjalanan.id_perjalanan, perjalanan.jam_berangkat, perjalanan.terminal, perjalanan.tarif, agency.nama_agency, agency.logo, bus.no_plat FROM `perjalanan`, `agency`, `bus` WHERE bus.no_plat=perjalanan.plat_no AND agency.id_agency=bus.agency_id AND perjalanan.asal_id=\''.$asal.'\' AND perjalanan.tujuan_id=\''.$tujuan.'\' AND perjalanan.tgl_pergi=\''.$tgl.'\' AND perjalanan.status_perjalanan=0');

        return $query;
    }

    function showOnePerjalanan($id)
    {
        $query = $this->db->kon->query('SELECT perjalanan.id_perjalanan, perjalanan.jam_berangkat, perjalanan.terminal, perjalanan.tarif, agency.nama_agency, agency.logo, bus.no_plat, bus.tipe, bus.kapasitas FROM `perjalanan`, `agency`, `bus` WHERE bus.no_plat=perjalanan.plat_no AND agency.id_agency=bus.agency_id AND perjalanan.id_perjalanan=\''.$id.'\' AND perjalanan.status_perjalanan=0');
        $hasil = $query->fetch_assoc();

        return $hasil;
    }

    function cekKursi($id)
    {
        $query = $this->db->kon->query('SELECT tiket.no_kursi, perjalanan.status_perjalanan FROM tiket, perjalanan WHERE tiket.perjalanan_id=perjalanan.id_perjalanan AND perjalanan.id_perjalanan=\''.$id.'\' AND perjalanan.status_perjalanan=0');

        return $query;
    }

//Transaksi
    function addTransaksi($kode, $nama, $email, $hp, $tgl, $jumlah, $tipe)
    {
        $query = $this->db->kon->query('INSERT INTO `transaksi`(`kode_trans`, `nama_pemesan`, `email`, `no_hp`, `tgl_trans`, `jumlah_penumpang`, `tipe_trans`) VALUES (\''.$kode.'\', \''.$nama.'\', \''.$email.'\', \''.$hp.'\', \''.$tgl.'\', \''.$jumlah.'\', \''.$tipe.'\')');

        return $query;
    }

    function cekTipeTrans($kode)
    {
        $query = $this->db->kon->query('SELECT tipe_trans FROM `transaksi` WHERE kode_trans=\''.$kode.'\'');
        $hasil = $query->fetch_assoc();

        return $hasil['tipe_trans'];
    }

    function getStatusBayar($kode)
    {
        $query = $this->db->kon->query('SELECT transaksi.status_bayar FROM transaksi WHERE transaksi.kode_trans=\''.$kode.'\'');
        $hasil = $query->fetch_assoc();

        return $hasil['status_bayar'];
    }

    function getDataTrans($kode){
        $query = $this->db->kon->query('SELECT DISTINCT agency.nama_agency, agency.logo, bus.no_plat, perjalanan.jam_berangkat, perjalanan.terminal, perjalanan.tgl_pergi FROM agency, bus, perjalanan, tiket WHERE tiket.trans_kode=\''.$kode.'\' AND perjalanan.id_perjalanan=tiket.perjalanan_id AND bus.no_plat=perjalanan.plat_no AND agency.id_agency=bus.agency_id');
        $hasil = $query->fetch_assoc();

        return $hasil;
    }

    function uploadBuktiTrans($kode, $foto){
        $query = $this->db->kon->query('UPDATE transaksi SET bukti_bayar=\''.$foto.'\', status_bayar=1 WHERE kode_trans=\''.$kode.'\'');

        return $query;
    }

    function cekTrans($kode){
        $query = $this->db->kon->query('SELECT nama_pemesan FROM `transaksi` WHERE kode_trans=\''.$kode.'\'');
        $hasil = $query->num_rows;

        return $hasil;
    }

//Tiket
    function addTiket($kode, $nama, $trans_kode, $id_p, $kursi)
    {
        $query = $this->db->kon->query('INSERT INTO `tiket`(`kode_tiket`, `nama_penumpang`, `trans_kode`, `perjalanan_id`, `no_kursi`) VALUES (\''.$kode.'\', \''.$nama.'\', \''.$trans_kode.'\', \''.$id_p.'\', \''.$kursi.'\')');

        return $query;
    }

    function getNamaPenumpang($kode)
    {
        $query = $this->db->kon->query('SELECT nama_penumpang, no_kursi FROM tiket WHERE trans_kode=\''.$kode.'\'');

        return $query;
    }

    function getTotalTrans($kode)
    {
        $query = $this->db->kon->query('SELECT SUM(perjalanan.tarif) AS total FROM perjalanan, tiket WHERE perjalanan.id_perjalanan=tiket.perjalanan_id AND tiket.trans_kode=\''.$kode.'\'');
        $hasil = $query->fetch_assoc();

        return $hasil['total'];
    }

    function getDataTiket($kode)
    {
        $query = $this->db->kon->query('SELECT tiket.kode_tiket, tiket.nama_penumpang, tiket.no_kursi, perjalanan.asal_id, perjalanan.tujuan_id FROM tiket, perjalanan WHERE tiket.trans_kode=\''.$kode.'\' AND perjalanan.id_perjalanan=tiket.perjalanan_id');

        return $query;
    }

//Bus
    function getPosisiBus($kode)
    {
        $query = $this->db->kon->query('SELECT bus.latitude, bus.longtitude, perjalanan.plat_no, tiket.perjalanan_id FROM bus,perjalanan,tiket WHERE bus.no_plat=perjalanan.plat_no AND tiket.trans_kode=\''.$kode.'\' AND tiket.perjalanan_id=perjalanan.id_perjalanan');
        $hasil = $query->fetch_assoc();

        return $hasil;
    }

//Umum

    function random_string($length) {
        $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'), range('A', 'Z'));

        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }

        return $key;
    }


}