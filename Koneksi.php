<?php
/**
 * Created by PhpStorm.
 * User: vedoalfarizi
 * Date: 19/08/2017
 * Time: 12:04
 */

class Koneksi
{
    public $kon;
    private $host   = 'localhost';
    private $user   = 'root';
    private $pass   = '';
    private $db     = 'tiketind';

    function __construct(){
        $this->kon = new mysqli($this->host, $this->user, $this->pass, $this->db);
    }
}