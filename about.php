<html>
<head>
    <title>About</title>
    <link href="bootstrap3/css/bootstrap.css" rel="stylesheet" />
    <link href="assets/css/ct-paper.css" rel="stylesheet"/>
    <link href="assets/css/demo.css" rel="stylesheet" />
    <link href="assets/css/examples.css" rel="stylesheet" />
    <link href="assets/css/paper-bootstrap-wizard.css" rel="stylesheet"/>
    <link rel="icon" type="image/png" href="assets/img/tiketind.png" />
    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/themify-icons.css" rel="stylesheet" type="text/css"/>

</head>
<body>

<!-- navigation -->
<nav class="navbar navbar-ct-neutral" role="navigation-demo" id="demo-navbar">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="http://www.creative-tim.com">
                <div class="logo-container">
                    <div class="logo">
                        <img src="assets/img/tiketind.png" class="img-responsive" alt="Tiketind">
                    </div>
                </div>
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navigation-example-2">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="index.php" class="btn btn-default btn-simple">Home</a>
                </li>
                <li>
                    <a href="about.php" class="btn btn-default btn-simple">About</a>
                </li>
                <li>
                    <a href="rent.php" class="btn btn-default btn-simple">Sewa/rental</a>
                </li>
                <li>
                    <a href="cek.php" class="btn btn-default btn-simple">Cek Transaksi</a>
                </li>
                <li>
                    <a href="bantuan.php" class="btn btn-default btn-simple">FAQ</a>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-->
</nav>
<!-- end navigation -->

<!-- Header -->
<div class="landing-header" style="background-image: url('assets/img/red.jpg');">
    <div class="container">
        <div class="motto">
            <img class="img-responsive" src="assets/img/tiketind.png" width="200"/>
            <h3>Ease Your Journey</h3>
            <br />
            <a href="#" class="btn" data-toggle="modal" data-target="#myModal"><i class="fa fa-play"></i>See video</a>
        </div>
    </div>
</div>
<!-- end header -->

<!-- modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="pull-right">
                    <a href="" data-dismiss="modal" aria-hidden="true">
                        <i class="ti-close text-danger"></i>
                    </a>
                </div>
                <div class="text-center text-default">
                    <h5>Video</h5>
                </div>
            </div>
            <div class="modal-body">
                <iframe width="500" height="300"
                        src="">
                </iframe>
            </div>
        </div>
    </div>
</div>
<!-- end modal -->

<!-- Tentang -->
<div class="section text-center landing-section">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h2>Tentang Kami</h2>
                <h5>Tiketind merupakan sebuah platform aplikasi berbasis web yang memudahkan perjalanan anda dengan membantu anda untuk mendapatkan tiket  untuk perjlanan anda dan penyewaan bus secara online.</h5>
                <br>
                <a href="index.php" class="btn btn-danger btn-fill">Pesan Sekarang</a>
            </div>
        </div>
    </div>
</div>
<!-- end tentang -->

<!-- Produk -->
<div class="section section-light-brown landing-section">
    <div class="container">
        <div class="row">
            <h2 class="text-center" >Produk Kami</h2>
            <div class="col-md-4 col-md-offset-1 text-center">
                    <h4>Pembelian tiket perjalanan</h4>
                    <img src="assets/img/tiketind.png" class="img-responsive text-center" width="200" style="margin-left: 20%"/>
                    <br>
                    <a class="btn btn-danger text-center" href="#">Pesan sekarang</a>
                </div>
            <div class="col-md-4 col-md-offset-2 text-center">
                    <h4>Penyewaan Bus</h4>
                    <img src="assets/img/tiketind.png" class="img-responsive text-center" width="200" style="margin-left: 20%"/>
                    <br>
                    <a class="btn btn-danger text-center" href="#">Pesan sekarang</a>
                </div>
        </div>
    </div>
</div>
<!-- endo produk -->

<!-- team -->
<div class="section section-dark text-center landing-section">
    <div class="container">
        <h2>Our Team</h2>
        <div class="col-md-4">
            <div class="team-player">
                <img src="assets/img/team/vedo.png" alt="Vedo Alfarizi" class="img-circle img-responsive">
                <h5>Vedo Alfarizi<br><small class="text-muted">Neo Team</small></h5>
            </div>
        </div>
        <div class="col-md-4">
            <div class="team-player">
                <img src="assets/img/team/trio.png" alt="M.Trio Saputra" class="img-circle img-responsive">
                <h5>M.Trio Saputra <br><small class="text-muted">Neo Team</small></h5>
            </div>
        </div>
        <div class="col-md-4">
            <div class="team-player">
                <img src="assets/img/team/ovi.png" alt="Ovi agfarisyah" class="img-circle img-circle img-responsive">
                <h5>Ovi Agfarisyah<br><small class="text-muted">Neo Team</small></h5>
            </div>
        </div>
    </div>
</div>
<!-- edn team -->

<!-- Kontak -->
<div class="section landing-section">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h2 class="text-center">Hubungi Kami</h2>
                <form class="contact-form">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Name</label>
                            <input class="form-control" placeholder="Name">
                        </div>
                        <div class="col-md-6">
                            <label>Email</label>
                            <input class="form-control" placeholder="Email">
                        </div>
                    </div>
                    <label>Message</label>
                    <textarea class="form-control" rows="4" placeholder="Tell us your thoughts and feelings..."></textarea>
                    <div class="row">
                        <div class="col-md-4 col-md-offset-4">
                            <button class="btn btn-danger btn-block btn-lg btn-fill">Send Message</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>
<!-- end kontak -->

<?php
    include 'template/footer.php';
?>