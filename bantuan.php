<html>
<head>
    <title>FAQ</title>
    <link href="bootstrap3/css/bootstrap.css" rel="stylesheet" />
    <link href="assets/css/ct-paper.css" rel="stylesheet"/>
    <link href="assets/css/demo.css" rel="stylesheet" />
    <link href="assets/css/examples.css" rel="stylesheet" />
    <link href="assets/css/paper-bootstrap-wizard.css" rel="stylesheet"/>
    <link rel="icon" type="image/png" href="assets/img/tiketind.png" />
    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/themify-icons.css" rel="stylesheet" type="text/css"/>
    <style>
        ul {
            margin: 0;
            padding: 0;
            list-style-type: none;
        }
    </style>
</head>
<body>

<!-- navigation -->
<nav class="navbar navbar-ct-neutral" role="navigation-demo" id="demo-navbar">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="http://www.creative-tim.com">
                <div class="logo-container">
                    <div class="logo">
                        <img src="assets/img/tiketind.png" class="img-responsive" alt="Tiketind">
                    </div>
                </div>
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navigation-example-2">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="index.php" class="btn btn-default btn-simple">Home</a>
                </li>
                <li>
                    <a href="about.php" class="btn btn-default btn-simple">About</a>
                </li>
                <li>
                    <a href="rent.php" class="btn btn-default btn-simple">Sewa/rental</a>
                </li>
                <li>
                    <a href="cek.php" class="btn btn-default btn-simple">Cek Transaksi</a>
                </li>
                <li>
                    <a href="bantuan.php" class="btn btn-default btn-simple">FAQ</a>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-->
</nav>
<!-- end navigation -->

<!-- According -->
<div class="section-dark-blue landing-section">
    <div class="container">
        <div class="text-center">
            <h4>FAQ</h4>
        </div>
        <div class="faq">
            <ul class="items">
                <li>
                    <a href="#" class="expanded">Bagaimana saya bisa memesan tiket?</a>
                    <ul class="sub-items" style="display: none;">
                        <li>
                            <p class="text-danger">Pertama, cari perjalanan. Kedua, booking. Ketiga, Isi biodata diri dan posisi duduk. Keempat, melakukan pembayaran beserta upload pembayaran. Kelima, print tiket anda</p>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#">Bagaimana cara jika saya salah memilih keberangkatan? </a>
                    <ul class="sub-items">
                        <li>
                            <p class="text-danger">Silahkan hubungi secepatnya, agar kami dapat memproses masalah anda dengan segera.</p>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#">Bagaimana agar saya dapat melihat apakah transaksi saya sudah di setujui oleh admin?</a>
                    <ul class="sub-items">
                        <li>
                            <p class="text-danger">Pilih menu cek transaksi, isi kode transaksi dan lihat status pembayaran jiak sudah payout maka admin sudah menerima pembayaran anda silahkan anda melakukan print tiket.</p>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#">Waktu tunggu pembayaran saya sudah lewat, apakah saya masih bisa melakukan pembayaran?</a>
                    <ul class="sub-items">
                        <li>
                            <p class="text-danger">Tidak bisa karena sistem kami akan menghapus transaksi anda. Jika anda melaukan pembayaran di luar waktu tenggang yang kami tentukan maka itu bukan tangguang jawab dari kami.</p>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#">Bagaimana cara agar saya bisa menjadi agency di Tikeind?</a>
                    <ul class="sub-items">
                        <li>
                            <p class="text-danger">Silahkan klik <a href="#" class="text-success"> disini</a></p>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- end according -->

<!-- footer -->
<footer class="footer-demo section-dark">
    <div class="container">
        <div class="copyright pull-right">
            ©Copyright 2017, all right reserved
        </div>
    </div>
</footer>

</body>
<script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="assets/js/jquery-ui-1.10.4.custom.min.js" type="text/javascript"></script>

<script src="bootstrap3/js/bootstrap.js" type="text/javascript"></script>

<!--  Plugins -->
<script src="assets/js/ct-paper-checkbox.js"></script>
<script src="assets/js/ct-paper-radio.js"></script>
<script src="assets/js/bootstrap-select.js"></script>
<script src="assets/js/bootstrap-datepicker.js"></script>
<script src="assets/js/jquery.datatables.min.js"></script>

<script src="assets/js/ct-paper.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#datatables').DataTable();
    });

    /*--- Booking dan Pencarian ---*/
    $('.cari').click(function(e){
        e.preventDefault();
        $("#cari").hide();
        $('#search').show();
    });

    $('#close').click(function(e){
        e.preventDefault();
        $("#search").hide();
        $('#cari').show();
    });

    $(".items > li > a").click(function(e) {
        e.preventDefault();
        var $this = $(this);
        if ($this.hasClass("expanded")) {
            $this.removeClass("expanded");
        } else {
            $(".items a.expanded").removeClass("expanded");
            $this.addClass("expanded");
            $(".sub-items").filter(":visible").slideUp("normal");
        }
        $this.parent().children("ul").stop(true, true).slideToggle("normal");
    });

    $(".sub-items a").click(function() {
        $(".sub-items a").removeClass("current");
        $(this).addClass("current");
    });
</script>
</html>