<?php
include 'Fungsi.php';
if((!isset($_POST['booking_tiket'])) && !isset($_POST['booking_bus'])){
    header('location:404.php');
}
$func = new Fungsi();
$id_p = $_POST['id_p'];
$jumlah_p = $_POST['jumlah_p'];

?>

<html>
<head>
    <title>Home</title>
    <link href="bootstrap3/css/bootstrap.css" rel="stylesheet" />
    <link href="assets/css/ct-paper.css" rel="stylesheet"/>
    <link href="assets/css/demo.css" rel="stylesheet" />
    <link href="assets/css/examples.css" rel="stylesheet" />
    <link href="assets/css/paper-bootstrap-wizard.css" rel="stylesheet"/>
    <link rel="icon" type="image/png" href="assets/img/tiketind.png" />
    <!--     Fonts and icons     -->
    <link href="assets/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/themify-icons.css" rel="stylesheet" type="text/css"/>

</head>
<body>
<!-- navigation -->
<nav class="navbar navbar-ct-neutral" role="navigation-demo" id="demo-navbar">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="">
                <div class="logo-container">
                    <div class="logo">
                        <img src="assets/img/tiketind.png" class="img-responsive" alt="Tiketind">
                    </div>
                </div>
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navigation-example-2">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="index.php" class="btn btn-default btn-simple">Home</a>
                </li>
                <li>
                    <a href="about.php" class="btn btn-default btn-simple">About</a>
                </li>
                <li>
                    <a href="rent.php" class="btn btn-default btn-simple">Sewa/rental</a>
                </li>
                <li>
                    <a href="cek.php" class="btn btn-default btn-simple">Cek Transaksi</a>
                </li>
                <li>
                    <a href="bantuan.php" class="btn btn-default btn-simple">FAQ</a>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-->
</nav>
<!-- end navigation -->

<!-- Info -->
    <?php
        if(isset($_POST['booking_tiket'])){
            $perjalanan = $func->showOnePerjalanan($id_p);
            echo '
        <div class="section">
            <div class="container">
                <div class="row">
                    <div class="container">
                        <div class="col-md-8">
                            <div class="tim-title">
                                <img src=\'assets/img/agency/'.$perjalanan['logo'].'\' width=\'100\' class=\'img-responsive\' />
                            </div>
                            <div class="col-md-6">
                                <h4>Agency</h4>
                                <h4>No plat</h4>
                                <h4>Waktu Berangkat</h4>
                                <h4>Terminal</h4>
                                <h4>Jumlah Penumpang</h4>
                                <h4>Tarif/orang</h4>
                                <a href="" onclick="history.back()" class="text-info">Ganti Armada</a>
                            </div>
                            <div class="col-md-6">
                                <h4>'.$perjalanan['nama_agency'].'</h4>
                                <h4>'.$perjalanan['no_plat'].'</h4>
                                <h4>'.$perjalanan['jam_berangkat'].' WIB</h4>
                                <h4>'.$perjalanan['terminal'].'</h4>
                                <h4>'.$jumlah_p.' Orang</h4>
                                <h4>Rp '.$perjalanan['tarif'].',00</h4>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <img src=\'assets/img/layout/'.$perjalanan['tipe'].'\' width=\'300\' class=\'img-responsive\' alt="\'Layout Bus\'"/>
                        </div>
                    </div>
                </div>  
            </div>
        </div>
        <!-- end info -->

        <!-- Data Diri -->
        <div class="section-thin">
            <div class="container">
                <div class="row">
                    <div class="tim-title">
                        <h3>Lengkapi Data</h3>
                    </div>
                    <div class="col-md-12 pull-left">
                        <form action="controller/bookingController.php" method="post">';
                        for($i=0; $i < $jumlah_p; $i++){
                            echo '
                                <div class="tim-title">
                                    <h5>Penumpang '.($i+1).'</h5>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Nama</label>
                                        <input type="text" name="nama[]" class="form-control" placeholder="Nama" required="">
                                    </div>
                                </div>
                            ';
                            if($i == 0){
                                echo '
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input type="email" name="email" class="form-control" placeholder="Tiket juga akan dikirimkan ke email ini" required="">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>No.Hp</label>
                                            <input type="text" name="no_hp" class="form-control" placeholder="No.Hp" required="">
                                        </div>
                                    </div>           
                                ';
                            }
                            echo '
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Pilih Kursi</label>
                                        <select name="no_kursi[]" class="form-control valid" required="">
                                            <option disabled selected value="">No Kursi Tersedia</option>';
                                            for($k=1; $k <= $perjalanan['kapasitas']; $k++){
                                                $tersedia = 1;
                                                $cek = $func->cekKursi($perjalanan['id_perjalanan']);
                                                while($no = $cek->fetch_assoc()){
                                                    if($k == $no['no_kursi']){
                                                        $tersedia = 0;
                                                    }
                                                }
                                                if($tersedia == 1){
                                                    echo '<option value=\''.$k.'\'>'.$k.'</option>';
                                                }
                                            }
                                        echo '</select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            ';
                        }
                        echo '
                            <input type=\'hidden\' name=\'id_p\' value=\''.$perjalanan['id_perjalanan'].'\'>
                            <input type = \'hidden\' name = \'jumlah_p\' value = \''.$jumlah_p.'\' >
                    ';

                    echo        '<button type="submit" name="booking" class="btn btn-danger btn-fill btn-lg btn-wd">Booking</button>
                        </form>
                    </div>
                    <div class="col-md-12">
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end data diri -->
            ';
        }else if(isset($_POST['booking_bus'])){
            echo '
                <div class="section">
                    <div class="container">';
                        $perjalanan = $func->showOnePerjalanan($id_p);
                        echo '
                            <div class="row">
                                <div class="container">
                                    <div class="col-md-8">
                                        <div class="tim-title">
                                            <img src=\'assets/img/agency/default.jpg\' width = \'100\' class=\'img-responsive\' />
                                        </div >
                                        <div class="col-md-6" >
                                            <h4></h4>
                                            <h4>Nama Agency</h4>
                                            <h4>NoPlat</h4>
                                            <h4> Seat</h4 >
                                            <h4> Rute</h4 >
                                            <h4> Tipe Bus </h4 >
                                            <h4> Tarif / hari</h4 >
                                            <h4></h4 >
                                            <a href = "" onclick = "history.back()" class="text-info" > Ganti Bus </a >
                                        </div >
                                        <div class="col-md-6" >
                                            <h4 > ANS</h4 >
                                            <h4 > BA 8081 LM </h4 >
                                            <h4 > 45 Kursi </h4 >
                                            <h4 > Sumbar</h4 >
                                            <h4 > Big Bus </h4 >
                                            <h4 > Rp 2.000.000,00</h4>
                                        </div>
                                    </div>
                                </div >
                            </div >
                            ';
                    echo '</div>
                </div>
                <!-- end info -->
                
                <!-- Data Diri -->
                <div class="section-thin">
                    <div class="container">
                        <div class="row">
                            <div class="tim-title">
                                <h3>Lengkapi Data</h3>
                            </div>
                            <div class="col-md-12 pull-left">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Nama</label>
                                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Nama" required="">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Email" required="">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>No.Hp</label>
                                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="No.Hp" required="">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Rute</label>
                                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Deskripsikan Rute Perjalanan Anda" required="">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <button class="btn btn-danger btn-fill btn-lg btn-wd">Pesan</button>
                            </div>
                            <div class="col-md-12">
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end data diri -->
                <!----------------------
            ';
        }
?>
<!-- footer -->
<footer class="footer-demo section-dark">
    <div class="container">
        <div class="copyright pull-right">
            ©Copyright 2017, all right reserved
        </div>
    </div>
</footer>

</body>
<script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="assets/js/jquery-ui-1.10.4.custom.min.js" type="text/javascript"></script>

<script src="bootstrap3/js/bootstrap.js" type="text/javascript"></script>

<!--  Plugins -->
<script src="assets/js/ct-paper-checkbox.js"></script>
<script src="assets/js/ct-paper-radio.js"></script>
<script src="assets/js/bootstrap-select.js"></script>
<script src="assets/js/bootstrap-datepicker.js"></script>
<script src="assets/js/jquery.datatables.min.js"></script>

<script src="assets/js/ct-paper.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#datatables').DataTable();
    });

    /*--- Booking dan Pencarian ---*/
    $('.cari').click(function(e){
        e.preventDefault();
        $("#cari").slideUp();
        $('#search').slideDown();
    });

    $('#close').click(function(e){
        e.preventDefault();
        $("#search").slideUp();
        $('#cari').slideDown();
    });
</script>
</html>