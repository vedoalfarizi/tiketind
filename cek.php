<?php
include "Fungsi.php";
$func = new Fungsi();

?>
<html>
<head>
    <title>Cek transaksi</title>
    <link href="bootstrap3/css/bootstrap.css" rel="stylesheet" />
    <link href="assets/css/ct-paper.css" rel="stylesheet"/>
    <link href="assets/css/demo.css" rel="stylesheet" />
    <link href="assets/css/examples.css" rel="stylesheet" />
    <link href="assets/css/paper-bootstrap-wizard.css" rel="stylesheet"/>
    <link rel="icon" type="image/png" href="assets/img/tiketind.png" />
    <!--     Fonts and icons     -->
    <link href="assets/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/themify-icons.css" rel="stylesheet" type="text/css"/>

</head>
<body>
<!-- navigation -->
<nav class="navbar navbar-ct-default" role="navigation-demo" id="demo-navbar">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="http://www.creative-tim.com">
                <div class="logo-container">
                    <div class="logo">
                        <img src="assets/img/tiketind.png" class="img-responsive" alt="Tiketind">
                    </div>
                </div>
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navigation-example-2">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="index.php" class="btn btn-default btn-simple">Home</a>
                </li>
                <li>
                    <a href="about.php" class="btn btn-default btn-simple">About</a>
                </li>
                <li>
                    <a href="rent.php" class="btn btn-default btn-simple">Sewa/rental</a>
                </li>
                <li>
                    <a href="cek.php" class="btn btn-default btn-simple">Cek Transaksi</a>
                </li>
                <li>
                    <a href="bantuan.php" class="btn btn-default btn-simple">FAQ</a>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-->
</nav>
<!-- end navigation -->

<!-- kode transaksi -->
<div class="section section-light-gray">
    <div class="container">
        <div class="row">
            <form action="" method="post">
                <div class="container">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Kode Transaksi</label>
                            <input type="text" name="kode" class="form-control" id="exampleInputEmail1" placeholder="Kode transaksi" required="">
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <input type="submit" name="cari_trans" class="btn btn-danger" id="exampleInputEmail1" value="Cari" required="">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end kode transaksi -->

<!------------------------
        Tiket Bus
-------------------------->

<?php
    if(isset($_GET['trans'])){
        $kode_trans = $_GET['trans'];
        $tipe = $func->cekTipeTrans($kode_trans);
        if($tipe == 0){
            echo '
    <!-- Tampilkan transaksi tiket-->
        <div class="section section-white">
            <div class="container">
                <div class="text-center tim-title">
                    <h2>Transaksi</h2>
                    <hr>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="col-md-6">
                                <h5><b>Kode Transaksi</b></h5>';
                            $penumpang = $func->getNamaPenumpang($kode_trans);
                            while($nama = $penumpang->fetch_assoc()){
                                echo '<h5><b>'.$nama['nama_penumpang'].'</b></h5>';
                            }
                        echo '  <h5>Total Transaksi</h5>
                                <h5>Status Pembayaran</h5>
                            </div>
                            <div class="col-md-6">
                                <h5><b>'.$kode_trans.'</b></h5>';
                            $penumpang = $func->getNamaPenumpang($kode_trans);
                            while($kursi = $penumpang->fetch_assoc()){
                                echo '<h5>Kursi '.$kursi['no_kursi'].'</h5>';
                            }
                            $total = $func->getTotalTrans($kode_trans);
                          echo '                                
                                <h5>Rp '.$total.'</h5>
                                <h5>Belum Bayar</h5>
                            </div>
                        </div>';
                        $dataTrans = $func->getDataTrans($kode_trans);
                        echo '<div class="col-md-4">
                            <div class="col-md-6">
                                <img src="assets/img/agency/'.$dataTrans['logo'].'" alt="logo agency" class="img-responsive"/>
                                <h5>Nama Agency</h5>
                                <h5>No plat</h5>
                                <h5>Waktu Berangkat</h5>
                                <h5>Terminal</h5>
                            </div>
                            <div class="col-md-6">
                                <b></b><br><br>
                                <h5>'.$dataTrans['nama_agency'].'</h5>
                                <h5>'.$dataTrans['no_plat'].'</h5>
                                <h5>'.$dataTrans['jam_berangkat'].'</h5>
                                <h5>'.$dataTrans['terminal'].'</h5>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="tim-title">
                                <h5>Anda memiliki waktu <b class="text-danger">60 Menit</b> untuk menyelesaikan pembayaran</h5>
                            </div>
                        </div>
                    </div>
                    <div class="tim-title">
                        <h4>Upload bukti pembayaran</h4>';
                        $status = $func->getStatusBayar($kode_trans);
                        if($status != 0){
                            echo '
                                <p class="text-info">Pembayaran Anda sedang kami proses, silahkan kembali dalam 20 Menit</p>
                            </div>
                            ';
                        }else{
                            echo '
                            <form action="controller/cekController.php" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="kode" value=\''.$kode_trans.'\'>
                                <input type="file" name="foto"/>
                                <br>
                                <input type="submit" name="upload_bukti" class="btn btn-danger btn-fill" value="Upload"/>
                            </form>';
                        }
                echo '</div>    
            </div>
        </div>
    <!-- End Transaksi -->      
            ';
        }else if($tipe == 1){
            echo '
                <!-- Tampilkan transaksi sewa-->
                    <div class="section section-white">
                        <div class="container">
                            <div class="text-center tim-title">
                                <h2>Transaksi</h2>
                                <hr>
                            </div>
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="col-md-6">
                                            <h5><b>Kode Transaksi</b></h5>
                                            <h5><b>Pemesan</b></h5>
                                            <h5>Total Transaksi</h5>
                                            <h5>Status Pembayaran</h5>
                                        </div>
                                        <div class="col-md-6">
                                            <h5><b>XXXGGHDG</b></h5>
                                            <h5>Azur</h5>
                                            <h5>Rp 3.000.000</h5>
                                            <h5>Belum Bayar</h5>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="col-md-6">
                                            <img src="assets/img/tiketind.jpg" alt="logo agency" class="img-responsive"/>
                                            <h5>Nama Agency</h5>
                                            <h5>No plat</h5>
                                            <h5>Tipe Bus</h5>
                                            <h5>Tarif/hari</h5>
                                        </div>
                                        <div class="col-md-6">
                                            <b></b><br><br>
                                            <h5>ANS</h5>
                                            <h5>BA 1029 BR</h5>
                                            <h5>Tipe A</h5>
                                            <h5>Rp 3.000.000</h5>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="tim-title">
                                            <h5>Sisa waktu pembayaran</h5>
                                        </div>
                                        <h4 class="text-danger">10.00</h4>
                                    </div>
                                </div>
                                <div class="tim-title">
                                    <h4>Upload bukti pembayaran</h4>
                                </div>
                                <input type="file"/>
                                <br>
                                <input type="submit" class="btn btn-danger btn-fill" value="Upload"/>
                            </div>
                        </div>
                    </div>
                <!-- End Transaksi -->
            ';
        }
    }else if(isset($_POST['cari_trans'])){
        $kode_trans = $_POST['kode'];
        $cek = $func->cekTrans($kode_trans);
        if($cek <= 0){
            echo '
                <div class="section-white">
                    <div class="container">
                        <div class="tim-title">
                            <div class="text-center tim-title">
                                <h5>Maaf, Kode Transaksi Tidak Ditemukan</h5>
                            </div>
                        </div>
                    </div>
                </div>
            ';
        }else{
            $tipe = $func->cekTipeTrans($kode_trans);
            $status = $func->getStatusBayar($kode_trans);
            if($tipe == 0){
                //tiket
                if($status == 0 || $status == 1){
                    echo '
    <!-- Tampilkan transaksi tiket-->
        <div class="section section-white">
            <div class="container">
                <div class="text-center tim-title">
                    <h2>Transaksi</h2>
                    <hr>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="col-md-6">
                                <h5><b>Kode Transaksi</b></h5>';
                    $penumpang = $func->getNamaPenumpang($kode_trans);
                    while($nama = $penumpang->fetch_assoc()){
                        echo '<h5><b>'.$nama['nama_penumpang'].'</b></h5>';
                    }
                    echo '  <h5>Total Transaksi</h5>
                                <h5>Status Pembayaran</h5>
                            </div>
                            <div class="col-md-6">
                                <h5><b>'.$kode_trans.'</b></h5>';
                    $penumpang = $func->getNamaPenumpang($kode_trans);
                    while($kursi = $penumpang->fetch_assoc()){
                        echo '<h5>Kursi '.$kursi['no_kursi'].'</h5>';
                    }
                    $total = $func->getTotalTrans($kode_trans);
                    echo '                                
                                <h5>Rp '.$total.'</h5>
                                <h5>Belum Bayar</h5>
                            </div>
                        </div>';
                    $dataTrans = $func->getDataTrans($kode_trans);
                    echo '<div class="col-md-4">
                            <div class="col-md-6">
                                <img src="assets/img/agency/'.$dataTrans['logo'].'" alt="logo agency" class="img-responsive"/>
                                <h5>Nama Agency</h5>
                                <h5>No plat</h5>
                                <h5>Waktu Berangkat</h5>
                                <h5>Terminal</h5>
                            </div>
                            <div class="col-md-6">
                                <b></b><br><br>
                                <h5>'.$dataTrans['nama_agency'].'</h5>
                                <h5>'.$dataTrans['no_plat'].'</h5>
                                <h5>'.$dataTrans['jam_berangkat'].'</h5>
                                <h5>'.$dataTrans['terminal'].'</h5>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="tim-title">
                                <h5>Anda memiliki waktu <b class="text-danger">60 Menit</b> untuk menyelesaikan pembayaran</h5>
                            </div>
                        </div>
                    </div>
                    <div class="tim-title">
                        <h4>Upload bukti pembayaran</h4>';
                    $status = $func->getStatusBayar($kode_trans);
                    if($status != 0){
                        echo '
                                <p class="text-info">Pembayaran Anda sedang kami proses, silahkan kembali dalam 20 Menit</p>
                            </div>
                            ';
                    }else{
                        echo '
                            <form action="controller/cekController.php" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="kode" value=\''.$kode_trans.'\'>
                                <input type="file" name="foto"/>
                                <br>
                                <input type="submit" name="upload_bukti" class="btn btn-danger btn-fill" value="Upload"/>
                            </form>';
                    }
                    echo '</div>    
            </div>
        </div>
    <!-- End Transaksi -->      
            ';
                }else if($status == 2){
                    echo '
                        <!-- Sudah payout tiket-->
                            <div class="section-white">
                                <div class="container">
                                    <div class="tim-title">
                                        <form action="controller/cekController.php" method="post">
                                            <input type="hidden" name="kode" value=\''.$kode_trans.'\'>
                                            <button formtarget="_blank" type="submit" name="print_tiket" class="btn btn-primary btn-fill">Print Tiket</button>
                                            <button type="submit" name="cek_posisi_tiket" class="btn btn-info btn-fill">Posisi bus</button>
                                        </form>
                                    </div>
                                    <div class="row">
                                        <div class="container">
                                            <div class="pull-right">
                                                <img class="img-responsive" src="assets/img/tiketind.png" width="100" alt="Tiketind">
                                            </div>
                                            <div class="row">
                                                <div class="container">
                                                    ';
                                                    $dataTiket = $func->getDataTiket($kode_trans);
                                                    $dataTrans = $func->getDataTrans($kode_trans);
                                                    while($r = $dataTiket->fetch_assoc()){
                                                  echo '<div class="col-md-6">
                                                            <div class="col-md-6">
                                                                <h5><b>Kode tiket</b></h5>
                                                                <h5>Nama</h5>
                                                                <h5>Dari</h5>
                                                                <h5>Ke</h5>
                                                                <h5>No.Kursi</h5>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <h5><b>'.$r['kode_tiket'].'</b></h5>
                                                                <h5><b>'.$r['nama_penumpang'].'</b></h5>
                                                                <h5>'.$func->getNamaKota($r['asal_id']).'</h5>
                                                                <h5>'.$func->getNamaKota($r['tujuan_id']).'</h5>
                                                                <h5>'.$r['no_kursi'].'</h5>
                                                            </div>
                                                            </div>
                                                        <div class="col-md-4">
                                                            <div class="text-center">
                                                                <h4 class="text-success"><u>Agency</u></h4>
                                                            </div>
                                                            <img class="img-responsive text-center" src="assets/img/agency/'.$dataTrans['logo'].'" width="80" alt="Logo agency/nama agency">
                                                            <div class="col-md-6">
                                                                <h5>No Plat</h5>
                                                                <h5>Tanggal</h5>
                                                                <h5>Berangkat Jam</h5>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <h5>'.$dataTrans['no_plat'].'</h5>
                                                                <h5>'.$dataTrans['tgl_pergi'].'</h5>
                                                                <h5>'.$dataTrans['jam_berangkat'].'</h5>
                                                            </div>
                                                        </div>
                                                        <!-- qr-code -->
                                                        <!--
                                                            <div class="col-md-2">
                                                                <img class="img-responsive" alt="QR-Code"/>
                                                            </div>
                                                        -->
                                                        <!-- end qr-code -->
                                                    </div>
                                                </div>                                            
                                                <hr>
                                            </div>';
                                                }
                                                    echo '
                                    </div>
                                </div>
                            </div>
                        <!-- end payout -->
                    ';
                }
            }else if($tipe == 1){
                //rental bus
                if($status == 0 || $status == 1){

                }else if($status == 2){
                    echo '
                        <!-- Sudah payout rental bus-->
                        <div class="section-white">
                            <div class="container">
                                <div class="tim-title">
                                    <button class="btn btn-primary btn-fill">Print Tiket</button>
                                    <button class="btn btn-info btn-fill">Posisi bus</button>
                                </div>
                                <div class="row">
                                    <div class="container">
                                        <div class="pull-right">
                                            <img class="img-responsive" src="assets/img/tiketind.jpg" width="100" alt="Tiketind">
                                        </div>
                                        <div class="row">
                                            <div class="container">
                                                <div class="col-md-6">
                                                    <div class="col-md-6">
                                                        <h5><b>Kode tiket</b></h5>
                                                        <h5>Pemesan</h5>
                                                        <h5>Berangkat</h5>
                                                        <h5>Durasi</h5>
                                                        <h5>Rute</h5>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <h5><b>XIUIKHJ90</b></h5>
                                                        <h5><b>Azur</b></h5>
                                                        <h5>Padang</h5>
                                                        <h5>5 Hari</h5>
                                                        <h5>Padang-Solok</h5>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="text-center">
                                                        <h4 class="text-success"><u>Agency</u></h4>
                                                    </div>
                                                    <img class="img-responsive text-center" src="assets/img/tiketind.jpg" width="80" alt="Logo agency/nama agency">
                                                    <div class="col-md-6">
                                                        <h5>No Plat</h5>
                                                        <h5>Jumlah Seat</h5>
                                                        <h5>Tipe Bus</h5>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <h5>BA 2002 BR</h5>
                                                        <h5>45 Kursi</h5>
                                                        <h5>Tipe A</h5>
                                                    </div>
                                                </div>
                                                <!-- qr-code -->
                                                <!--
                                                    <div class="col-md-2">
                                                        <img class="img-responsive" alt="QR-Code"/>
                                                    </div>
                                                -->
                                                <!-- end qr-code -->
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="pull-right">
                                            <img class="img-responsive" src="assets/img/tiketind.jpg" width="100" alt="Tiketind">
                                        </div>
                                        <div class="row">
                                            <div class="container">
                                                <div class="col-md-6">
                                                    <div class="col-md-6">
                                                        <h5><b>Kode tiket</b></h5>
                                                        <h5>Pemesan</h5>
                                                        <h5>Berangkat</h5>
                                                        <h5>Durasi</h5>
                                                        <h5>Rute</h5>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <h5><b>XIUIKHJ90</b></h5>
                                                        <h5><b>Ayu</b></h5>
                                                        <h5>Bukittinggi</h5>
                                                        <h5>2 Hari</h5>
                                                        <h5>Keliling Sumbar</h5>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="text-center">
                                                        <h4 class="text-success"><u>Agency</u></h4>
                                                    </div>
                                                    <img class="img-responsive text-center" src="assets/img/tiketind.jpg" width="80" alt="Logo agency/nama agency">
                                                    <div class="col-md-6">
                                                        <h5>No Plat</h5>
                                                        <h5>Jumlah Seat</h5>
                                                        <h5>Tipe Bus</h5>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <h5>BA 2002 BR</h5>
                                                        <h5>50 Kursi</h5>
                                                        <h5>Tipe B</h5>
                                                    </div>
                                                </div>
                                                <!-- qr-code -->
                                                <!--
                                                    <div class="col-md-2">
                                                        <img class="img-responsive" alt="QR-Code"/>
                                                    </div>
                                                -->
                                                <!-- end qr-code -->
                                            </div>
                                        </div>
                                        <hr>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end payout -->      
                                ';
                }
            }
        }
    }
?>
<!-- footer -->
<footer class="footer-demo section-dark">
    <div class="container">
        <div class="copyright pull-right">
            ©Copyright 2017, all right reserved
        </div>
    </div>
</footer>

</body>
<script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="assets/js/jquery-ui-1.10.4.custom.min.js" type="text/javascript"></script>

<script src="bootstrap3/js/bootstrap.js" type="text/javascript"></script>

<!--  Plugins -->
<script src="assets/js/ct-paper-checkbox.js"></script>
<script src="assets/js/ct-paper-radio.js"></script>
<script src="assets/js/bootstrap-select.js"></script>
<script src="assets/js/bootstrap-datepicker.js"></script>
<script src="assets/js/jquery.datatables.min.js"></script>

<script src="assets/js/ct-paper.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#datatables').DataTable();
    });

    /*--- Booking dan Pencarian ---*/
    $('.cari').click(function(e){
        e.preventDefault();
        $("#cari").slideUp();
        $('#search').slideDown();
    });

    $('#close').click(function(e){
        e.preventDefault();
        $("#search").slideUp();
        $('#cari').slideDown();
    });
</script>
</html>