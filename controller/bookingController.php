<?php
/**
 * Created by PhpStorm.
 * User: vedoalfarizi
 * Date: 19/08/2017
 * Time: 21:33
 */

include "../Fungsi.php";
$func = new Fungsi();

if(isset($_POST['booking'])){
    $nama       = $_POST['nama'];
    $email      = $_POST['email'];
    $no_hp      = $_POST['no_hp'];
    $no_kursi   = $_POST['no_kursi'];
    $id_p       = $_POST['id_p'];
    $jumlah_p   = $_POST['jumlah_p'];
    $kode_trans = $func->random_string(6);
    $tgl        = date("m/d/Y");

    $transaksi = $func->addTransaksi($kode_trans, $nama[0], $email, $no_hp, $tgl, $jumlah_p, 0);
    if($transaksi){
        for($i=0; $i < $jumlah_p; $i++){
            $kode_tiket = $func->random_string(6);
            $tiket = $func->addTiket($kode_tiket, $nama[$i], $kode_trans, $id_p, $no_kursi[$i]);
        }
        header('location:../cek.php?trans='.$kode_trans.'');
    }
}
?>