<?php
/**
 * Created by PhpStorm.
 * User: vedoalfarizi
 * Date: 20/08/2017
 * Time: 1:14
 */
include "../Fungsi.php";
$func = new Fungsi();

if(isset($_POST['upload_bukti'])){
    $kode = $_POST['kode'];

    $fileName = $_FILES["foto"]["name"];
    $fileTmpLoc = $_FILES["foto"]["tmp_name"];
    $fileType = $_FILES["foto"]["type"];
    $fileSize = $_FILES["foto"]["size"];
    $fileErrorMsg = $_FILES["foto"]["error"];

    if(!$fileTmpLoc){
        echo "Belum ada file yang dipilih";
    }
    if($fileSize > 5242880) {
        echo "Ukuran file yang anda masukkan terlalu besar!<br>Ukuran maksimal peng-upload-tan foto 2 MB";
    }
    if (!preg_match("/.(gif|jpg|png)$/i", $fileName) ) {
        echo "File yang anda masukkan bukan bertipe gambar!";
    }
    if ($fileErrorMsg == 1) { // if file upload error key is equal to 1
        echo "Terjadi masalah dalam peng-upload-an gambar!";
    }

    $ext = pathinfo($fileName, PATHINFO_EXTENSION);
    $newName = $kode.".".$ext;
    $moveResult = move_uploaded_file($fileTmpLoc, "../assets/img/bukti/$newName");

    if ($moveResult != true) {
       echo "Gagal mengupload foto <br>";
    }else {
        $upload = $func->uploadBuktiTrans($kode, $newName);
        if ($upload == true) {
            header('Location: ' . $_SERVER["HTTP_REFERER"]);
        } else {
            echo "Ada kesalahan saat menambahkan data <br>";
        }

    }
}

if(isset($_POST['print_tiket'])){
    $kode = $_POST['kode'];
    header('location:../tiket.php?kode='.$kode.'');
}

if(isset($_POST['cek_posisi_tiket'])){
    $kode = $_POST['kode'];
    $posisi = $func->getPosisiBus($kode);

    header('location:../lokasi.php?lat='.$posisi['latitude'].'&long='.$posisi['longtitude'].'');
}
?>