<?php
    include 'Fungsi.php';

    $func = new Fungsi();
?>
<html>
<head>
    <title>Home</title>
    <link href="bootstrap3/css/bootstrap.css" rel="stylesheet" />
    <link href="assets/css/ct-paper.css" rel="stylesheet"/>
    <link href="assets/css/demo.css" rel="stylesheet" />
    <link href="assets/css/examples.css" rel="stylesheet" />
    <link href="assets/css/paper-bootstrap-wizard.css" rel="stylesheet"/>
    <link href="assets/img/tiketind.png" rel="icon" type="image/png" />
    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/themify-icons.css" rel="stylesheet" type="text/css"/>

</head>
<body>
<!-- navigation -->
<nav class="navbar navbar-ct-neutral" role="navigation-demo" id="demo-navbar">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="http://www.creative-tim.com">
                <div class="logo-container">
                    <div class="logo">
                        <img src="assets/img/tiketind.png" class="img-responsive" alt="Tiketind">
                    </div>
                </div>
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navigation-example-2">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="index.php" class="btn btn-default btn-simple">Home</a>
                </li>
                <li>
                    <a href="about.php" class="btn btn-default btn-simple">About</a>
                </li>
                <li>
                    <a href="rent.php" class="btn btn-default btn-simple">Sewa/rental</a>
                </li>
                <li>
                    <a href="cek.php" class="btn btn-default btn-simple">Cek Transaksi</a>
                </li>
                <li>
                    <a href="bantuan.php" class="btn btn-default btn-simple">FAQ</a>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-->
</nav>
<!-- end navigation -->

<!-- Slide -->
<div id="carousel"><!-- Slide -->
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <div class="carousel slide" data-ride="carousel">

            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
                <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item active">
                    <img src="assets/img/tiketind.png" alt="Awesome Image">
                </div>
                <div class="item">
                    <img src="assets/img/tiketind.png" alt="Awesome Image">
                </div>
                <div class="item">
                    <img src="assets/img/tiketind.png" alt="Awesome Image">
                </div>
            </div>

            <!-- Controls -->
            <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                <span class="fa fa-angle-left"></span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                <span class="fa fa-angle-right"></span>
            </a>
        </div>
    </div>
</div>
<!-- end slide -->

<!-- Pencarian -->
<div class="section-gray container-fluid" id="cari">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <!--      Wizard container        -->
            <div class="wizard-container">
                <div class="card wizard-card" data-color="green">
                    <form action="#search" method="post">
                        <div class="wizard-header">
                            <h3 class="wizard-title">Pesan Bus</h3>
                        </div>
                        <div class="wizard-navigation">
                            <ul class="nav nav-pills">
                                <li class="active text-center" style="width: 100%;">
                                </li>
                            </ul>
                        </div>
                        <div class="tab-content">
                            <div class="tab-pane active">
                                <div class="row">
                                        <div class="col-sm-12">
                                            <h5 class="info-text"> </h5>
                                        </div>
                                        <div class="col-sm-5 col-sm-offset-1">
                                            <div class="form-group">
                                                <label>Berangkat</label>
                                                <select name="dari" class="form-control valid" required="">
                                                    <option disabled selected value="">Piih Kota Asal</option>
                                                <?php
                                                    $kotas = $func->showAllKota();
                                                    while($r = $kotas->fetch_assoc()){
                                                        echo '
                                                            <option value=\''.$r['id_kota'].'\'>'.$r['nama_kota'].'</option>
                                                        ';
                                                    }
                                                ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-5">
                                            <div class="form-group">
                                                <label>Tujuan</label>
                                                <select name="ke" class="form-control valid" required="">
                                                    <option disabled selected value="">Piih Kota Tujuan</option>
                                                    <?php
                                                    $kotas = $func->showAllKota();
                                                    while($r = $kotas->fetch_assoc()){
                                                        echo '
                                                            <option value=\''.$r['id_kota'].'\'>'.$r['nama_kota'].'</option>
                                                        ';
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-5 col-sm-offset-1">
                                            <div class="form-group">
                                                <label>Jumlah Penumpang</label>
                                                <div class="input-group">
                                                    <input type="text" name="jp" class="form-control" placeholder="Jumlah Penumpang" required="">
                                                    <span class="input-group-addon"><i class="ti-user"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-5">
                                            <div class="form-group">
                                                <label>Tanggal</label>
                                                <div class="input-group">
                                                    <input type="text" name="tgl" class="datepicker form-control" placeholder="Tanggal" required="">
                                                    <span class="input-group-addon"><i class="ti-calendar"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                        <div class="wizard-footer">
                            <div class="text-center">
                                <input type="submit" class="btn btn-next btn-fill btn-success btn-wd" name="cari" value="Cari">
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </form>
                </div>
            </div> <!-- wizard container -->
        </div>
    </div> <!-- row -->
</div>
<!-- End pencarian -->

<?php
    if(isset($_POST['cari'])){
        $asal = $_POST['dari'];
        $tujuan = $_POST['ke'];
        $penumpang = $_POST['jp'];
        $tgl = $_POST['tgl'];

        $perjalanans = $func->showPerjalanan($asal, $tujuan, $tgl);
        echo '
               <!-- Hasil Pencarian -->
                <div class="section container-fluid" id="search">
                    <div class="row">
                        <div class="container">
                            <div class="col-md-12">
                                <div class="pull-left">
                                    <h2>Pencarian</h2>
                                </div>
                                <div class="pull-right">
                                    <a id="close" class="btn btn-danger btn-simple ">x</a>
                                </div>
                            </div>
                            <div class="col-md-12">
                            <table id="datatables" class="table table-striped" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Nama Agency</th>
                                    <th>No Plat</th>
                                    <th>Waktu Berangkat</th>
                                    <th>Terminal</th>
                                    <th>Tarif/orang</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>';
                        while($r = $perjalanans->fetch_assoc()){
                            echo '  <tr>
                                    <td><img src=\'assets/img/agency/'.$r['logo'].'\' width=\'100\' class=\'img-responsive\'></td>
                                    <td>'.$r['nama_agency'].'</td>
                                    <td>'.$r['no_plat'].'</td>
                                    <td>'.$r['jam_berangkat'].'</td>
                                    <td>'.$r['terminal'].'</td>
                                    <td>'.$r['tarif'].'</td>
                                <form action=\'booking.php\' method=\'post\'>
                                    <input type="hidden" name=\'id_p\' value=\''.$r['id_perjalanan'].'\'>
                                    <input type="hidden" name=\'jumlah_p\' value=\''.$penumpang.'\'>
                                    <td><input type=\'submit\' name="booking_tiket" class=\'btn btn-warning btn-small\' value=\'Booking\'></td>
                                </form>
                                </tr>';
                        }
                        echo '
                                </tbody>
                            </table>
                        </div>
                        </div>
                    </div>
                </div>
                <!-- end hasil pencarian -->
        ';
    }
?>

<!-- footer -->
<footer class="footer-demo section-dark">
    <div class="container">
        <div class="copyright pull-right">
            ©Copyright 2017, all right reserved
        </div>
    </div>
</footer>

</body>
<script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="assets/js/jquery-ui-1.10.4.custom.min.js" type="text/javascript"></script>

<script src="bootstrap3/js/bootstrap.js" type="text/javascript"></script>

<!--  Plugins -->
<script src="assets/js/ct-paper-checkbox.js"></script>
<script src="assets/js/ct-paper-radio.js"></script>
<script src="assets/js/bootstrap-select.js"></script>
<script src="assets/js/bootstrap-datepicker.js"></script>
<script src="assets/js/jquery.datatables.min.js"></script>

<script src="assets/js/ct-paper.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#datatables').DataTable();
    });

    /*--- Booking dan Pencarian ---*/
    $('.cari').click(function(e){
        e.preventDefault();
        $("#cari").slideUp();
        $('#search').slideDown();
    });

    $('#close').click(function(e){
        e.preventDefault();
        $("#search").slideUp();
        $('#cari').slideDown();
    });
</script>
</html>