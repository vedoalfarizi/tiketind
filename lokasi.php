<?php
include 'Fungsi.php';
$func = new Fungsi();
if(isset($_GET['lat']) && isset($_GET['long'])){
    $lat = $_GET['lat'];
    $long = $_GET['long'];
}else{
    header('location:404.php');
}
?>

<html>
<head>
    <title>Cek transaksi</title>
    <link href="bootstrap3/css/bootstrap.css" rel="stylesheet" />
    <link href="assets/css/ct-paper.css" rel="stylesheet"/>
    <link href="assets/css/demo.css" rel="stylesheet" />
    <link href="assets/css/examples.css" rel="stylesheet" />
    <link href="assets/css/paper-bootstrap-wizard.css" rel="stylesheet"/>
    <link rel="icon" type="image/png" href="assets/img/tiketind.png" />
    <!--     Fonts and icons     -->
    <link href="assets/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/themify-icons.css" rel="stylesheet" type="text/css"/>

</head>
<body>
<!-- navigation -->
<nav class="navbar navbar-ct-default" role="navigation-demo" id="demo-navbar">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="http://www.creative-tim.com">
                <div class="logo-container">
                    <div class="logo">
                        <img src="assets/img/tiketind.png" class="img-responsive" alt="Tiketind">
                    </div>
                </div>
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navigation-example-2">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="index.php" class="btn btn-default btn-simple">Home</a>
                </li>
                <li>
                    <a href="about.php" class="btn btn-default btn-simple">About</a>
                </li>
                <li>
                    <a href="rent.php" class="btn btn-default btn-simple">Sewa/rental</a>
                </li>
                <li>
                    <a href="cek.php" class="btn btn-default btn-simple">Cek Transaksi</a>
                </li>
                <li>
                    <a href="bantuan.php" class="btn btn-default btn-simple">FAQ</a>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-->
</nav>
<!-- end navigation -->

<div class="section-gray landing-section">
    <div class="container">
        <div class="tim-title text-center">
            <h3>Lokasi Bus</h3>
        </div>
        <?php
            echo '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1736.4494735655167!2d'.$lat.'!3d'.$long.'!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2fd4b8cac88d22f5%3A0xe9d79d78c04bf94e!2sPO.+Ayah!5e0!3m2!1sid!2sid!4v1503174952298" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>';
        ?>
    </div>
</div>
<!-- footer -->
<footer class="footer-demo section-dark">
    <div class="container">
        <div class="copyright pull-right">
            ©Copyright 2017, all right reserved
        </div>
    </div>
</footer>

</body>
<script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="assets/js/jquery-ui-1.10.4.custom.min.js" type="text/javascript"></script>

<script src="bootstrap3/js/bootstrap.js" type="text/javascript"></script>

<!--  Plugins -->
<script src="assets/js/ct-paper-checkbox.js"></script>
<script src="assets/js/ct-paper-radio.js"></script>
<script src="assets/js/bootstrap-select.js"></script>
<script src="assets/js/bootstrap-datepicker.js"></script>
<script src="assets/js/jquery.datatables.min.js"></script>

<script src="assets/js/ct-paper.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#datatables').DataTable();
    });

    /*--- Booking dan Pencarian ---*/
    $('.cari').click(function(e){
        e.preventDefault();
        $("#cari").slideUp();
        $('#search').slideDown();
    });

    $('#close').click(function(e){
        e.preventDefault();
        $("#search").slideUp();
        $('#cari').slideDown();
    });
</script>
</html>