<?php
    include 'Fungsi.php';
    $func = new Fungsi();
?>
<html>
<head>
    <title>Print Tiket</title>
    <link href="bootstrap3/css/bootstrap.css" rel="stylesheet" />
    <link href="assets/css/ct-paper.css" rel="stylesheet"/>
    <link href="assets/css/demo.css" rel="stylesheet" />
    <link href="assets/css/examples.css" rel="stylesheet" />
    <link href="assets/css/paper-bootstrap-wizard.css" rel="stylesheet"/>
    <link rel="icon" type="image/png" href="assets/img/tiketind.png" />
    <!--     Fonts and icons     -->
    <link href="assets/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/themify-icons.css" rel="stylesheet" type="text/css"/>

</head>
<body onload="window.print()">
<div class="section section-with-space section-white">
    <div class="container">
    <?php
        $kode = $_GET['kode'];
        $tipe = $func->cekTipeTrans($kode);
        if($tipe == 0){
            $dataTiket = $func->getDataTiket($kode);
            $dataTrans = $func->getDataTrans($kode);
            while($r = $dataTiket->fetch_assoc()){
                echo '
                        <div class="pull-right">
                            <img class="img-responsive" src="assets/img/tiketind.png" width="100" alt="Tiketind">
                        </div>
                        <div class="row">
                            <div class="col-md-10">
        
                                <h5>
                                    <b>Kode tiket</b>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <b>'.$r['kode_tiket'].'</b>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <u style="margin-left: 15%">Agency</u>
                                </h5>
                                <h5>
                                    Nama
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <b>'.$r['nama_penumpang'].'</b>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <img style="margin-left: 15%;" src="assets/img/agency/'.$dataTrans['logo'].'" width="80" alt="Logo agency/nama agency">
                                </h5>
                                <h5>
                                    Dari
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    '.$func->getNamaKota($r['asal_id']).'
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    No.Plat
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    '.$dataTrans['no_plat'].'
                                </h5>
                                <h5>
                                    Ke
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    '.$func->getNamaKota($r['tujuan_id']).'
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    Tanggal
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    '.$dataTrans['tgl_pergi'].'
                                </h5>
                                <h5>
                                    No.Kursi
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    '.$r['no_kursi'].'
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    Berangkat Jam
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    '.$dataTrans['jam_berangkat'].'
                                </h5>
        
                        </div>
                    <!-- qr-code -->
                        <!--
                           <div class="col-md-2">
                             <img class="img-responsive" alt="QR-Code"/>
                           </div>
                        -->
                    <!-- end qr-code -->
                </div>
                <hr>
                ';
            }
        }else if($tipe == 1){
            $dataTiket = $func->getDataTiket($kode);
            $dataTrans = $func->getDataTrans($kode);
            while($r = $dataTiket->fetch_assoc()){
                echo '
                <div class="pull-right">
                    <img class="img-responsive" src="assets/img/tiketind.png" width="100" alt="Tiketind">
                </div>
                <div class="row">
                    <div class="col-md-10">

                        <h5>
                            <b>Kode tiket</b>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <b>'.$r['kode_tiket'].'</b>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <u style="margin-left: 15%">Agency</u>
                        </h5>
                        <h5>
                            Nama
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <b>Bagus</b>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <img style="margin-left: 15%;" src="assets/img/tiketind.jpg" width="80" alt="Logo agency/nama agency">
                        </h5>
                        <h5>
                            Dari
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            Padang
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            No.Plat
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            BA 2028 AR
                        </h5>
                        <h5>
                            Rute
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            Padang-Pekanbaru
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            Durasi
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            3 Hari
                        </h5>
                        <h5>
                            Jumlah seat
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            51 Kursi
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;
        
                        </h5>
        
                    </div>
                    <!-- qr-code -->
                    <!--
                       <div class="col-md-2">
                         <img class="img-responsive" alt="QR-Code"/>
                       </div>
                    -->
                    <!-- end qr-code -->
                </div>
                <hr>
                ';
            }
        }
    ?>
    </div>
</div>

</body>
<script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="assets/js/jquery-ui-1.10.4.custom.min.js" type="text/javascript"></script>

<script src="bootstrap3/js/bootstrap.js" type="text/javascript"></script>

<!--  Plugins -->
<script src="assets/js/ct-paper-checkbox.js"></script>
<script src="assets/js/ct-paper-radio.js"></script>
<script src="assets/js/bootstrap-select.js"></script>
<script src="assets/js/bootstrap-datepicker.js"></script>
<script src="assets/js/jquery.datatables.min.js"></script>

<script src="assets/js/ct-paper.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#datatables').DataTable();
    });

    /*--- Booking dan Pencarian ---*/
    $('.cari').click(function(e){
        e.preventDefault();
        $("#cari").slideUp();
        $('#search').slideDown();
    });

    $('#close').click(function(e){
        e.preventDefault();
        $("#search").slideUp();
        $('#cari').slideDown();
    });
</script>
</html>