-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 19, 2017 at 01:07 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tiketind`
--

-- --------------------------------------------------------

--
-- Table structure for table `agency`
--

CREATE TABLE `agency` (
  `id_agency` int(11) NOT NULL,
  `nama_pemilik` varchar(255) NOT NULL,
  `nama_agency` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `agency`
--

INSERT INTO `agency` (`id_agency`, `nama_pemilik`, `nama_agency`, `alamat`, `logo`, `email`, `password`) VALUES
(1, 'vedo alfarizi', 'AYAH', 'Ulak Karang Padang', 'default.jpg', 'vedoalfarizi@gmail.com', 'e10adc3949ba59abbe56e057f20f883e'),
(2, 'Trio', 'Sinamar', 'Padang', 'default.jpg', 'trio@gmail.com', 'e10adc3949ba59abbe56e057f20f883e');

-- --------------------------------------------------------

--
-- Table structure for table `bus`
--

CREATE TABLE `bus` (
  `no_plat` varchar(15) NOT NULL,
  `agency_id` int(11) NOT NULL,
  `kapasitas` tinyint(4) NOT NULL,
  `tipe` varchar(255) NOT NULL,
  `latitude` double DEFAULT NULL,
  `longtitude` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bus`
--

INSERT INTO `bus` (`no_plat`, `agency_id`, `kapasitas`, `tipe`, `latitude`, `longtitude`) VALUES
('BA 5140 CI', 1, 16, '1.png', NULL, NULL),
('BA 777 CA', 2, 27, '2.png', NULL, NULL),
('BA 999 N', 1, 12, '2.png', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `kota`
--

CREATE TABLE `kota` (
  `id_kota` mediumint(8) UNSIGNED NOT NULL,
  `nama_kota` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kota`
--

INSERT INTO `kota` (`id_kota`, `nama_kota`) VALUES
(1, 'Padang'),
(2, 'Padang Panjang'),
(3, 'Bukittinggi'),
(4, 'Payakumbuh');

-- --------------------------------------------------------

--
-- Table structure for table `perjalanan`
--

CREATE TABLE `perjalanan` (
  `id_perjalanan` int(11) NOT NULL,
  `plat_no` varchar(10) NOT NULL,
  `asal_id` mediumint(8) UNSIGNED NOT NULL,
  `tujuan_id` mediumint(8) UNSIGNED NOT NULL,
  `terminal` varchar(255) NOT NULL,
  `tarif` int(10) UNSIGNED NOT NULL,
  `jam_berangkat` varchar(255) NOT NULL,
  `tgl_pergi` varchar(20) NOT NULL,
  `status_perjalanan` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `jumlah` tinyint(3) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `perjalanan`
--

INSERT INTO `perjalanan` (`id_perjalanan`, `plat_no`, `asal_id`, `tujuan_id`, `terminal`, `tarif`, `jam_berangkat`, `tgl_pergi`, `status_perjalanan`, `jumlah`) VALUES
(1, 'BA 999 N', 1, 2, 'Ulak Karang', 18000, '14.00', '09/15/2017', 0, 0),
(2, 'BA 5140 CI', 1, 4, 'Ulak Karang', 25000, '08.00', '08/30/2017', 0, 0),
(3, 'BA 777 CA', 2, 4, 'Silaiang', 18000, '10.00', '08/21/2017', 0, 0),
(4, 'BA 777 CA', 1, 2, 'Aia Pacah', 18000, '10.00', '09/15/2017', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tiket`
--

CREATE TABLE `tiket` (
  `kode_tiket` varchar(12) NOT NULL,
  `nama_penumpang` varchar(255) NOT NULL,
  `trans_kode` varchar(12) NOT NULL,
  `perjalanan_id` int(11) NOT NULL,
  `no_kursi` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tiket`
--

INSERT INTO `tiket` (`kode_tiket`, `nama_penumpang`, `trans_kode`, `perjalanan_id`, `no_kursi`) VALUES
('TIKET01', 'Ovira', 'UD12AS', 1, '2A'),
('TIKET02', 'Oyong', 'UD12AS', 1, '2B');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `kode_trans` varchar(12) NOT NULL,
  `nama_pemesan` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `no_hp` varchar(15) NOT NULL,
  `tgl_trans` date NOT NULL,
  `jumlah_penumpang` tinyint(4) NOT NULL,
  `status_bayar` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `bukti_bayar` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`kode_trans`, `nama_pemesan`, `email`, `no_hp`, `tgl_trans`, `jumlah_penumpang`, `status_bayar`, `bukti_bayar`) VALUES
('UD12AS', 'M. Trio Saputra', 'triosaputra@gmail.com', '081236471211', '2017-08-18', 2, 0, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `agency`
--
ALTER TABLE `agency`
  ADD PRIMARY KEY (`id_agency`);

--
-- Indexes for table `bus`
--
ALTER TABLE `bus`
  ADD PRIMARY KEY (`no_plat`),
  ADD KEY `agency_id` (`agency_id`);

--
-- Indexes for table `kota`
--
ALTER TABLE `kota`
  ADD PRIMARY KEY (`id_kota`);

--
-- Indexes for table `perjalanan`
--
ALTER TABLE `perjalanan`
  ADD PRIMARY KEY (`id_perjalanan`),
  ADD KEY `plat_no` (`plat_no`),
  ADD KEY `asal_id` (`asal_id`),
  ADD KEY `tujuan_id` (`tujuan_id`);

--
-- Indexes for table `tiket`
--
ALTER TABLE `tiket`
  ADD PRIMARY KEY (`kode_tiket`),
  ADD KEY `trans_kode` (`trans_kode`),
  ADD KEY `perjalanan_id` (`perjalanan_id`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`kode_trans`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `agency`
--
ALTER TABLE `agency`
  MODIFY `id_agency` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `kota`
--
ALTER TABLE `kota`
  MODIFY `id_kota` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `perjalanan`
--
ALTER TABLE `perjalanan`
  MODIFY `id_perjalanan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `bus`
--
ALTER TABLE `bus`
  ADD CONSTRAINT `bus_ibfk_1` FOREIGN KEY (`agency_id`) REFERENCES `agency` (`id_agency`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `perjalanan`
--
ALTER TABLE `perjalanan`
  ADD CONSTRAINT `perjalanan_ibfk_1` FOREIGN KEY (`asal_id`) REFERENCES `kota` (`id_kota`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `perjalanan_ibfk_2` FOREIGN KEY (`tujuan_id`) REFERENCES `kota` (`id_kota`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `perjalanan_ibfk_3` FOREIGN KEY (`plat_no`) REFERENCES `bus` (`no_plat`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tiket`
--
ALTER TABLE `tiket`
  ADD CONSTRAINT `tiket_ibfk_1` FOREIGN KEY (`trans_kode`) REFERENCES `transaksi` (`kode_trans`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tiket_ibfk_2` FOREIGN KEY (`perjalanan_id`) REFERENCES `perjalanan` (`id_perjalanan`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
